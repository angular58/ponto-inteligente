#### Criando o projeto
```
npm new ponto-inteligente
```
#### Instalando o material
```
npm install --save @angular/material @angular/cdk @angular/animations hammerjs @angular/flex-layout
```
serviço na pasta target
/usr/lib/jvm/java-8-oracle/bin/java -jar ponto-inteligente-0.0.1-SNAPSHOT.jar



#### Instalando Moment.js
```
npm install moment --save
```

#### geração de dominio Ex.: primeiro, cria-se o módulo, depois os componentes
```
ng g module funcionario
ng g component funcionario/component/lancamento
ng g component funcionario/component/listagem
```
#### gerando pipe
```
ng g pipe shared/pipe/tipo
```

#### Configurando CORS no angular
na raiz do projeto criar um arquivo chamado proxy.conf.json
```
{
  "/api": {
    "target": "http://localhost:8080",
    "secure": false
  }
}
```
e no arquivo package.json em "start": "ng serve" adicionar o arquivo ficando assim:
```
"start": "ng serve proxy.conf.json"
```
Para subir a aplicação trocar o comando "ng serve" por "npm start" (o resultado será o mesmo)
como tem o proxy configurado, pode-se trocar a url dos serviços em enviroment que o proxy redirecionará
para a url certa.

#### Produção (diretório dist)
```
ng build --prod
```
