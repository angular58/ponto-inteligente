import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CadastroPfComponent} from './component/cadastro-pf.component';
import {CadastrarPfComponent} from './component/cadastrar-pf/cadastrar-pf.component';

export const CadastroPfRoutes: Routes = [
  {
    path: 'cadastro-pf',
    component: CadastroPfComponent,
    children: [
      {
        path: '', component: CadastrarPfComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(CadastroPfRoutes)],
  exports: [RouterModule]
})
export class CadastroPfRoutingModule { // é importado no módulo principal AppModule

}
