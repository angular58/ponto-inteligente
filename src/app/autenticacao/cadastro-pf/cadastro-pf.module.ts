import {NgModule} from '@angular/core';
import {CadastrarPfComponent} from './component/cadastrar-pf/cadastrar-pf.component';
import {CommonModule} from '@angular/common';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule} from '@angular/router';
import {CadastroPfComponent} from './component/cadastro-pf.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {CadastrarPfService} from './services/cadastrar-pf.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatTooltipModule,
    MatSnackBarModule,
    SharedModule
  ],
  declarations: [
    CadastrarPfComponent,
    CadastroPfComponent
  ],
  exports: [
    CadastrarPfComponent,
    CadastroPfComponent
  ],
  providers: [
    CadastrarPfService
  ]
})
export class CadastroPfModule {

}
