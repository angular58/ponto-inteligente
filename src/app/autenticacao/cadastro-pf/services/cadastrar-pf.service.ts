import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {CadastroPfModel} from '../component/model/cadastro-pf.model';
import {Observable} from 'rxjs';

@Injectable()
export class CadastrarPfService {

  private readonly PATH: string = 'cadastrar-pf';

  constructor(private http: HttpClient) { }

  cadastrar(cadastroPf: CadastroPfModel): Observable<any> {
    return this.http.post(environment.baseAPiUrl + this.PATH, cadastroPf);
  }

}
