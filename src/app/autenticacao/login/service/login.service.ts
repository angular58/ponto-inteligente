import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginModel} from '../model/login.model';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class LoginService {

  private readonly PATH: string = 'auth';

  constructor(private http: HttpClient) {}

  logar(login: LoginModel): Observable<any> {
    return this.http.post(environment.baseUrl + this.PATH, login);
  }

}
