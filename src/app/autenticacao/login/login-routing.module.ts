import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LogarComponent} from './component/logar.component';
import {LoginComponent} from './component/login/loginComponent';

export const LoginRoutes: Routes = [
  {
    path: 'login',
    component: LogarComponent,
    children: [{path: '', component: LoginComponent}]
  }
]

@NgModule({
  imports: [RouterModule.forChild(LoginRoutes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { // é importado no módulo principal AppModule
}
