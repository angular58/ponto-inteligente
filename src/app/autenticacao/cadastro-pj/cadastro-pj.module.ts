import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CadastroPjComponent} from './component/cadastro-pj.component';
import {RouterModule} from '@angular/router';
import {FlexLayoutModule} from '@angular/flex-layout';
import {CadastrarPjComponent} from './component/cadastrar-pj/cadastrar-pj.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {
  MatButtonModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatTooltipModule,
  MatSnackBarModule
} from '@angular/material';
import {SharedModule} from '../../shared/shared.module';
import {CadastrarPjService} from './service/cadastrar-pj.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatTooltipModule,
    MatSnackBarModule,
    SharedModule
  ],
  declarations: [
    CadastroPjComponent,
    CadastrarPjComponent
  ],
  exports: [
    CadastroPjComponent,
    CadastrarPjComponent
  ],
  providers: [
    CadastrarPjService
  ]
})
export class CadastroPjModule {

}
