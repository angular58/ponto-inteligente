import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CadastroPjModel} from '../model/cadastro-pj.model';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class CadastrarPjService {

  private readonly PATH: string = 'cadastrar-pj';

  constructor (private http: HttpClient) { }

  cadastrar(cadastroPj: CadastroPjModel): Observable<any> {
    return this.http.post(environment.baseAPiUrl + this.PATH, cadastroPj);
  }

}
