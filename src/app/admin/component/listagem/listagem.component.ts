import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatSelect,
  MatSnackBar,
  MatTableDataSource,
  PageEvent,
  Sort
} from '@angular/material';
import {LancamentoModel} from '../../../funcionario/component/model/lancamento.model';
import {LancamentoService} from '../../../shared/service/lancamento.service';
import {HttpUtilService} from '../../../shared/service/http-util.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {FuncionarioModel} from '../../../shared/model/funcionario.model';
import {FuncionarioService} from '../../../shared/service/funcionario.service';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ListagemComponent implements OnInit {

  dataSource: MatTableDataSource<LancamentoModel>;
  colunas: string[] = ['data', 'tipo', 'localizacao', 'acao'];
  funcionarioId: string;
  totalLancamentos: number;

  funcionarios: FuncionarioModel[];
  @ViewChild(MatSelect) matSelect: MatSelect;
  form: FormGroup;

  private pagina: number;
  private ordem: string;
  private direcao: string;

  constructor(
    private lancamentoService: LancamentoService,
    private httpUtil: HttpUtilService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private funcionarioService: FuncionarioService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.pagina = 0;
    this.ordemPadrao();
    this.obterFuncionarios();
    this.gerarForm();
  }

  gerarForm() {
    this.form = this.fb.group({
      funcs: ['', []]
    });
  }

  get funcId(): string {
    return sessionStorage['funcionarioId'] || false;
  }

  obterFuncionarios() {
    this.funcionarioService.listarFuncionariosPorEmpresa()
      .subscribe(data => {
        const usuarioId: string = this.httpUtil.obterIdUsuario();
        this.funcionarios = (data['data'] as FuncionarioModel[])
          .filter(func => func.id != usuarioId);
        if (this.funcId) {
          this.form.get('funcs').setValue(parseInt(this.funcId, 10));
          this.exibirLancamentos();
        }
      },
      err => {
        const msg: string = 'Erro obtendo funcionaários.';
        this.snackBar.open(msg, 'Erro', {duration: 5000});
      });
  }

  ordemPadrao() {
    this.ordem = 'data';
    this.direcao = 'DESC';
  }

  exibirLancamentos() {
    if (this.matSelect.selected) {
      this.funcionarioId = this.matSelect.selected['value'];
    } else if(this.funcId) {
      this.funcionarioId = this.funcId;
    } else {
      return;
    }

    sessionStorage['funcionarioId'] = this.funcionarioId;

    this.lancamentoService.listarLancamentosPorFuncionario(
      this.funcionarioId, this.pagina.toString(), this.ordem, this.direcao
    ).subscribe(data => {
      this.totalLancamentos = data['data'].totalElements;
      const lancamentos = data['data'].content as LancamentoModel[];
      this.dataSource = new MatTableDataSource<LancamentoModel>(lancamentos);
    },
    err => {
      const msg: string = 'Erro obtendo lançamentos.';
      this.snackBar.open(msg, 'Erro', {duration: 5000});
    });
  }

  paginar(pageEvent: PageEvent) {
    this.pagina = pageEvent.pageIndex;
    this.exibirLancamentos();
  }

  ordenar(sort: Sort) {
    if (sort.direction == '') {
      this.ordemPadrao();
    } else {
      this.ordem = sort.active;
      this.direcao = sort.direction.toUpperCase();
    }
    this.exibirLancamentos();
  }

  removerDialog(lancamentoId: string) {
    const dialog = this.dialog.open(ConfirmDialog, {});
    dialog.afterClosed().subscribe(remover => {
      if (remover) {
        this.remover(lancamentoId);
      }
    })
  }

  remover(lancamentoId: string) {
    this.lancamentoService.remover(lancamentoId)
      .subscribe(data => {
        this.exibirLancamentos();
        const msg: string = 'Lançamento removido com sucesso!';
        this.snackBar.open(msg, 'Sucesso', {duration: 50000});
      },
      err => {
        let msg: string = 'Tente novamente em instantes.';
        if (err.status == 400) {
          msg = err.error.errors.join(' ');
        }
        this.snackBar.open(msg, 'Erro', {duration: 5000});
      });
  }

}

@Component({
  selector: 'confirm-dialog',
  template: `
    <h1 mat-dialog-title>
      Deseja realmente remover o lançamento?
    </h1>
    <div mat-dialog-actions>
      <button mat-button [mat-dialog-close]="false" tabindex="1-">
        Não
      </button>
      <button mat-button [mat-dialog-close]="true" tabindex="2">
        Sim
      </button>
    </div>
  `
})
export class ConfirmDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any){ }
}
