import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AdminComponent} from './component/admin.component';
import {ListagemComponent} from './component/listagem/listagem.component';
import {CadastroComponent} from './component/cadastro/cadastro.component';
import {AtualizacaoComponent} from './component/atualizacao/atualizacao.component';
import {AdminGuardService} from './service/admin-guard.service';

export const AdminRoutes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [ AdminGuardService ],
    children: [
      {
        path: '', component: ListagemComponent
      },
      {
        path: 'cadastro', component: CadastroComponent
      },
      {
        path: 'atualizacao/:lancamentoId', component: AtualizacaoComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(AdminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {

}
