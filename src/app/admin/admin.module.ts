import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmDialog, ListagemComponent} from './component/listagem/listagem.component';
import {CadastroComponent} from './component/cadastro/cadastro.component';
import {AtualizacaoComponent} from './component/atualizacao/atualizacao.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule} from '@angular/router';
import {AdminComponent} from './component/admin.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
  MatButtonModule,
  MatDatepickerModule,
  MatDialogModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatNativeDateModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import {SharedModule} from '../shared/shared.module';
import {LancamentoService} from '../shared/service/lancamento.service';
import {HttpUtilService} from '../shared/service/http-util.service';
import {PtBrMatPaginatorInitl} from '../shared/service/pt-br-mat-paginator-initl';
import {FuncionarioService} from '../shared/service/funcionario.service';
import {AdminGuardService} from './service/admin-guard.service';

@NgModule({
  declarations: [
    ListagemComponent,
    CadastroComponent,
    AtualizacaoComponent,
    AdminComponent,
    ConfirmDialog
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatTooltipModule,
    MatIconModule,
    MatSnackBarModule,
    MatTableModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatPaginatorModule,
    MatSortModule,
    SharedModule
  ],
  exports: [
    ListagemComponent,
    CadastroComponent,
    AtualizacaoComponent,
    AdminComponent,
    ConfirmDialog
  ],
  providers: [
    LancamentoService,
    HttpUtilService,
    MatPaginatorIntl,
    FuncionarioService,
    AdminGuardService,
    // {provide: MAT_DATE_LOCALE, useValue: 'pt-BR'},
    {provide: MatPaginatorIntl, useClass: PtBrMatPaginatorInitl}
  ],
  entryComponents: [ConfirmDialog] //permite o angular chamar o componente sem ser no html
})
export class AdminModule { }
