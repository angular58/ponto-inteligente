import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {HttpUtilService} from '../../shared/service/http-util.service';
import {Observable} from 'rxjs';

@Injectable()
export class AdminGuardService implements CanActivate {

  constructor(
    private httpUtil: HttpUtilService,
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.httpUtil.obterPerfil() === 'ROLE_ADMIN') {
      return true;
    }
    this.router.navigate(['/funcionario']);
    return false;
  }

}
