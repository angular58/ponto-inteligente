import {Pipe, PipeTransform} from '@angular/core';
import {TipoEnum} from '../model/tipo.enum';

@Pipe({
  name: 'tipo'
})
export class TipoPipe implements PipeTransform {

  transform(tipo: TipoEnum, args?: any): string {
    return this.obterTexto(tipo);
  }

  obterTexto(tipo: TipoEnum): string {
    let tipoDesc: string;

    switch (tipo) {
      case TipoEnum.INICIO_TRABALHO:
        tipoDesc = 'Início do trabalho'
          break;
      case TipoEnum.INICIO_ALMOCO:
        tipoDesc = 'Início do alomoço';
        break;
      case TipoEnum.TERMINO_ALMOCO:
        tipoDesc = 'Término do almoço';
        break;
      case TipoEnum.TERMINO_TRABALHO:
        tipoDesc = 'Término do trabalho';
        break;
      default:
        tipoDesc = tipo;
    }

    return tipoDesc;

  }

}
