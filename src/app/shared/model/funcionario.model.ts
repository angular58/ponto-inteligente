import {LancamentoModel} from '../../funcionario/component/model/lancamento.model';

export class FuncionarioModel {
  constructor(
    public nome: string,
    public email: string,
    public cpf: string,
    public perfil: string,
    public valorHora?: string,
    public qtdHorasTrabalhoDia?: string,
    public qtdHorasAlmoco?: string,
    public lancamentos?: LancamentoModel[],
    public id?: string
  ) { }
}
