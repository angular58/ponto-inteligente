import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MascaraDirective} from './directive/mascara/mascara.directive';
import {PtBrMatPaginatorInitl} from './service/pt-br-mat-paginator-initl';
import { TipoPipe } from './pipe/tipo.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MascaraDirective,
    TipoPipe
  ],
  exports: [
    MascaraDirective,
    TipoPipe
  ],
  providers: [
    PtBrMatPaginatorInitl
  ]
})
export class SharedModule {

}
