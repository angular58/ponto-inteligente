import {Injectable} from '@angular/core';
import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class HttpUtilService {

  constructor() { }

  headers() {
    let httpHeaders: HttpHeaders = new HttpHeaders();
    if (localStorage['token']) {
      httpHeaders = httpHeaders.set('Authorization', 'Bearer ' + localStorage['token']);
    }
    return {headers: httpHeaders};
  }

  obterIdUsuario(): string {
    if (!localStorage['token']) {
      return '';
    }
    const dadosUsuario = this.obterDadosUsuario();
    return dadosUsuario ? dadosUsuario.id :'';
  }

  obterIdEmpresa(): string {
    if (!localStorage['token']) {
      return '';
    }
    const dadosUsuario = JSON.parse(atob(localStorage['token'].split('.')[1]));
    return dadosUsuario ? dadosUsuario.empresaId : '';
  }

  obterDadosUsuario() {
    if (!localStorage['token']) {
      return '';
    }
    return JSON.parse(atob(localStorage['token'].split('.')[1]));
  }

  obterPerfil(): string {
    if (!localStorage['token']) return''
    const dadosUsuarios = JSON.parse(atob(localStorage['token'].split('.')[1]));
    return dadosUsuarios ? dadosUsuarios.role : '';
  }

}
