import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HttpUtilService} from './http-util.service';
import {environment} from '../../../environments/environment';

@Injectable()
export class FuncionarioService {

  private readonly PATH: string = 'funcionarios';
  private readonly PATH_FUNC_POR_EMPRESA = '/empresa/{empresaId}';

  constructor(
    private http: HttpClient,
    private httpUtil: HttpUtilService
  ) { }

  listarFuncionariosPorEmpresa() {
    return this.http.get(environment.baseAPiUrl + this.PATH +
      this.PATH_FUNC_POR_EMPRESA.replace('{empresaId}', this.httpUtil.obterIdEmpresa),
        this.httpUtil.headers());
  }

}
