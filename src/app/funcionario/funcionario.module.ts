import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListagemComponent} from './component/listagem/listagem.component';
import {LancamentoComponent} from './component/lancamento/lancamento.component';
import {FuncionarioComponent} from './component/funcionario.component';
import {RouterModule} from '@angular/router';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatPaginatorIntl,
  MatPaginatorModule,
  MatSortModule,
  MatTableModule,
  MatTooltipModule
} from '@angular/material';
import {MatSnackBarModule} from '@angular/material/typings/esm5/snack-bar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpUtilService} from '../shared/service/http-util.service';
import {LancamentoService} from '../shared/service/lancamento.service';
import {PtBrMatPaginatorInitl} from '../shared/service/pt-br-mat-paginator-initl';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    ListagemComponent,
    LancamentoComponent,
    FuncionarioComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatTooltipModule,
    MatIconModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    SharedModule
  ],
  exports: [
    FuncionarioComponent
  ],
  providers: [
    HttpUtilService,
    LancamentoService,
    {provide: MatPaginatorIntl, useClass: PtBrMatPaginatorInitl}
  ]
})
export class FuncionarioModule { }
