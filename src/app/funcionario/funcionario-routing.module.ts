import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FuncionarioComponent} from './component/funcionario.component';
import {LancamentoComponent} from './component/lancamento/lancamento.component';
import {ListagemComponent} from './component/listagem/listagem.component';

export const FuncionarioRoutes: Routes = [
  {
    path: 'funcionario',
    component: FuncionarioComponent,
    children: [
      {
        path: '', component: LancamentoComponent
      },
      {
        path: 'listagem', component: ListagemComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forChild(FuncionarioRoutes)],
  exports: [RouterModule]
})
export class FuncionarioRoutingModule { // é importado no módulo principal AppModule

}
