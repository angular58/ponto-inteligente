import {Component, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';

import * as moment from 'moment';
import {Router} from '@angular/router';
import {TipoEnum} from '../../../shared/model/tipo.enum';
import {HttpUtilService} from '../../../shared/service/http-util.service';
import {LancamentoService} from '../../../shared/service/lancamento.service';
import {LancamentoModel} from '../model/lancamento.model';

/*
Para usar geolocalização do navegador.
'declare var' significa que existe um objeto nativo
fora do contexto do angular que vc deseja
que seja incluída na aplicação.
*/
declare var navigator: any;

@Component({
  selector: 'app-lancamento',
  templateUrl: './lancamento.component.html',
  styleUrls: ['./lancamento.component.css']
})
export class LancamentoComponent implements OnInit {

  private dataAtualEn: string;
  dataAtual: string;
  geoLocation: string;
  ultimoTipoLancado: string;

  constructor(
    private snackBar: MatSnackBar,
    private router: Router,
    private httpUtil: HttpUtilService,
    private lancamentoService: LancamentoService
  ) { }

  ngOnInit() {
    this.dataAtual = moment().format('DD/MM/YYYY HH:mm:ss');
    this.dataAtualEn = moment().format('YYYY-MM-DD HH:mm:ss');
    this.obterGeoLocation();
    this.ultimoTipoLancado = '';
    this.obterUltimoLancamento();
  }

  iniciarTrabalho() {
    this.cadastrar(TipoEnum.INICIO_TRABALHO);
  }

  terminarTrabalho() {
    this.cadastrar(TipoEnum.TERMINO_TRABALHO);
  }

  iniciarAlmoco() {
    this.cadastrar(TipoEnum.INICIO_ALMOCO);
  }

  terminarAlmoco() {
    this.cadastrar(TipoEnum.TERMINO_ALMOCO);
  }

  obterUltimoLancamento() {
    this.lancamentoService.buscarUltimoTipoLancado()
      .subscribe(data => {
        this.ultimoTipoLancado = data.data ? data.data.tipo : '';
      },
        err => {
        const msg: string = 'Erro obtendo último lançamento.';
        this.snackBar.open(msg, 'Erro', {duration: 5000});
        });
  }

  cadastrar(tipo: TipoEnum) {
    const lancamento: LancamentoModel = new LancamentoModel(
      this.dataAtualEn, tipo, this.geoLocation, this.httpUtil.obterIdUsuario()
    );

    this.lancamentoService.cadastrar(lancamento)
      .subscribe(data => {
        const msg: string = 'Lançamento realizado com sucesso!';
        this.snackBar.open(msg, 'Sucesso', {duration: 5000});
        this.router.navigate(['/funcionario/listagem']);
      },
      err => {
        let msg: string = 'Tente novamento em instantes.';
        if (err.status == 400) {
          msg = err.error.errors.join(' ');
        }
        this.snackBar.open(msg, 'Erro', {duration: 5000});
      })

  }

  obterGeoLocation(): string {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position =>
      this.geoLocation = `${position.coords.latitude},${position.coords.longitude}`);
    }
    return '';
  }

  obterUrlMapa(): string {
    return 'https://www.google.com/maps/search/?api=1&query=' + this.geoLocation;
  }

  exibirInicioTrabalho(): boolean {
    return this.ultimoTipoLancado == '' ||
      this.ultimoTipoLancado == TipoEnum.TERMINO_TRABALHO
  }

  exibirTerminoTrabalho(): boolean {
    return this.ultimoTipoLancado == TipoEnum.INICIO_TRABALHO ||
      this.ultimoTipoLancado == TipoEnum.TERMINO_ALMOCO;
  }

  exibirInicioAlmoco(): boolean {
    return this.ultimoTipoLancado == TipoEnum.INICIO_TRABALHO;
  }

  exibirTerminoAlmoco(): boolean {
    return this.ultimoTipoLancado == TipoEnum.INICIO_ALMOCO;
  }

}
