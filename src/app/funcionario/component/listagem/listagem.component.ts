import {Component, OnInit, ViewChild} from '@angular/core';

// import 'rxjs/add/observable/of';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {LancamentoModel} from '../model/lancamento.model';
import {LancamentoService} from '../../../shared/service/lancamento.service';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ListagemComponent implements OnInit {

  dataSource: MatTableDataSource<LancamentoModel>;
  colunas: string[] = ['data', 'tipo', 'localizacao'];

  // viewchild -> obtem instancia desse objeto no html
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private lancamentoService: LancamentoService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.lancamentoService.listarTodosLancamentos()
      .subscribe(data => {
        const lancamentos = data['data'] as LancamentoModel[];
        this.dataSource = new MatTableDataSource<LancamentoModel>(lancamentos);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      err => {
        const msg: string = 'Erro obtendo lançamentos.';
        this.snackBar.open(msg, 'Erro', {duration: 5000});
      });
  }

}
